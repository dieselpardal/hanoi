# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import time

from hanoi import hanoi


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.



if __name__ == '__main__':
    print_hi('HANOI POR PYTHON!')
    ini = time.time()
    x = hanoi()
    x.setPiece(3)
    x.count_total()
    #x.total(30)
    fim = time.time()
    print(f'Tempo de execucao: {int(fim - ini)} segundos.')

