class hanoi():
    count = 0
    piece =0
    show = False

    def setPiece(self, piece):
        self.piece = piece
        self.count = 1

    def run_hanoi(self,n,f,h,t):
        if n>0:
            self.run_hanoi(n-1, f, t, h)
            if(self.show):
                self.move(f,t)
            self.count = self.count + 1
            self.run_hanoi(n-1,h,f,t)

    def start(self):
        self.show = True
        self.run_hanoi(self.piece, 'A', 'B', 'C')

    def total(self, piece):
        for i in range(1, piece + 1):
            print(f'Piece {i}:', end="")
            self.setPiece(i)
            self.count_total()

    def count_total(self):
        self.show = True
        self.run_hanoi(self.piece, 'A', 'B', 'C')
        print(f'Total of {self.count - 1} moves.')

    def move(self,f,t):
        print(f'{self.count}) Move disc from {f} to {t}')